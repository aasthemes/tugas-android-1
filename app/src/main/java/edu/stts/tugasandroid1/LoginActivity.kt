package edu.stts.tugasandroid1

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

class LoginActivity : AppCompatActivity(), LoginTopFragment.LoginTopActionListener {

    private lateinit var loginBottomFragment: LoginBottomFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initLoginTopFragment()
        initLoginBottomFragment()
    }

    private fun initLoginTopFragment() {
        // Get the text fragment instance
        val loginTopFragment = LoginTopFragment.newInstance(this)

        // Get the support fragment manager instance
        val manager = supportFragmentManager

        // Begin the fragment transition using support fragment manager
        val transaction = manager.beginTransaction()

        // Replace the fragment on container
        transaction.replace(R.id.fragment_login_top,loginTopFragment)
        transaction.addToBackStack(null)

        // Finishing the transition
        transaction.commit()
    }

    private fun initLoginBottomFragment() {
        // Get the text fragment instance
        loginBottomFragment = LoginBottomFragment.newInstance()

        // Get the support fragment manager instance
        val manager = supportFragmentManager

        // Begin the fragment transition using support fragment manager
        val transaction = manager.beginTransaction()

        // Replace the fragment on container
        transaction.replace(R.id.fragment_login_bottom,loginBottomFragment)
        transaction.addToBackStack(null)

        // Finishing the transition
        transaction.commit()
    }

    override fun onUserReady(user: User) {
        loginBottomFragment.receiveUser(user)
    }

}